<?php

namespace Hyrioo\LaravelHyperModel\Rules;

use Hyrioo\LaravelHyperModel\Models\Model;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Contracts\Validation\Factory;

class Relationship implements Rule
{
    /**
     * @var Model
     */
    private $model;
    /**
     * @var Model
     */
    private $relation;

    public function __construct($model, $relation)
    {
        $this->model = $model;
        $this->relation = $relation;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $class = get_class((new $this->model)->$attribute());
        if($class == BelongsToMany::class)
        {
            foreach ($value as $item){
                $this->passesItem($item);
            }
        }else{
            $this->passesItem($value);
        }

        return true;
    }

    private function passesItem($item)
    {
        if(is_array($item)) {
            $keyName = (new $this->relation)->getKeyName();
            if(array_key_exists($keyName, $item)) {
                $id = $item[$keyName];
                ($this->relation)::findOrFail($id);
                $this->getValidationFactory()
                    ->make($item, ($this->relation)::patchRules($id))
                    ->validate();
            }else{
                $this->getValidationFactory()
                    ->make($item, ($this->relation)::storeRules())
                    ->validate();
            }
        }else{
            ($this->relation)::findOrFail($item);
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'test';
    }

    /**
     * Get a validation factory instance.
     *
     * @return \Illuminate\Contracts\Validation\Factory
     */
    protected function getValidationFactory()
    {
        return app(Factory::class);
    }
}