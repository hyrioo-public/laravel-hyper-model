<?php

namespace Hyrioo\LaravelHyperModel\Traits;

use Hyrioo\LaravelHyperModel\ModelTransformer;
use Illuminate\Database\Eloquent\Collection;

trait Transformation
{
    /**
     * The default transformer
     * @var array
     */
    public $transformer = null;


    private function hasTransformer()
    {
        return $this->isTransformer($this->transformer);
    }

    private static function isTransformer($transformer)
    {
        return $transformer != null && is_subclass_of($transformer, ModelTransformer::class);
    }

    public function transform(ModelTransformer $transformer = null)
    {
        if (!$this->hasTransformer() && !self::isTransformer($transformer)) {
            return $this->toArray();
        }

        return $transformer != null ? $transformer::transform($this) : $this->transformer::transform($this);
    }

    public static function transformCollection(Collection $models, ModelTransformer $transformer)
    {
        if (!self::isTransformer($transformer)) {
            return $models->toArray();
        }

        return $transformer::transformCollection($models);
    }
}