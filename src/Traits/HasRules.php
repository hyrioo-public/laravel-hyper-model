<?php

namespace Hyrioo\LaravelHyperModel\Traits;

use Hyrioo\LaravelHyperModel\ModelTransformer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Validation\Rules\Unique;

trait HasRules
{

    /**
     * Basic rules definition, must work for both store and patch
     * @return array
     */
    public static function baseRules()
    {
        return [];
    }

    /**
     * Rules to apply when storing a new instance
     * @return array
     */
    public static function storeRules()
    {
        return self::baseRules();
    }

    /**
     * Rules to apply when patching an existing instance
     * @return array
     */
    public static function patchRules($modelId)
    {
        return self::baseRules();
    }

    /**
     * Append child rules
     * @param $baseRules array
     * @param $newRules array Rules to append
     * @param $key string Key to append rules to. Eg use home for adding home.name
     * @return array
     */
    public static function appendRules($baseRules, $newRules, $key)
    {
        foreach ($newRules as $ruleKey => $rule) {
            $baseRules[$key . '.' . $ruleKey] = $rule;
        }

        return $baseRules;
    }

    /**
     * Adds required validator if not present, for all keys
     * @param $baseRules array
     * @param $keys array Rules to be made required
     * @return array
     */
    public static function makeRulesRequired($baseRules, $keys = true)
    {
        if ($keys === true) {
            foreach ($baseRules as $key => $rule) {
                if(is_string($baseRules[$key])){
                    $validators = explode('|', $baseRules[$key]);
                    if(!in_array('required', $validators)){
                        $baseRules[$key] = 'required|'.$baseRules[$key];
                    }
                }else{
                    if(!in_array('required', $baseRules[$key])){
                        $baseRules[$key][] = 'required';
                    }
                }
            }
        } else {
            foreach ($keys as $key) {
                if(is_string($baseRules[$key])){
                    $validators = explode('|', $baseRules[$key]);
                    if(!in_array('required', $validators)){
                        $baseRules[$key] = 'required|'.$baseRules[$key];
                    }
                }else{
                    if(!in_array('required', $baseRules[$key])){
                        $baseRules[$key][] = 'required';
                    }
                }
            }
        }

        return $baseRules;
    }

    protected static function IgnoreUniqueId($rules, $id, $column = null)
    {
        if(is_string($rules)){
            $validators = explode('|', $rules);
            foreach($validators as $key => $rule){
                if(strrpos($rule, 'unique:') == 0){
                    $validators[$key] .= ','.$id;
                }
            }
            $rules = implode($validators, '|');
        }else{
            foreach($rules as $key => $rule){
                if(is_string($rule) && strrpos($rule, 'unique:') === 0){
                    $rules[$key] .= ','.$id;
                }else if(is_object($rule) && get_class($rule) == Unique::class){
                    /** @var Unique $rule */
                    if($column == null){
                        $rule->ignore($id);
                    }else{
                        $rule->ignore($id, $column);
                    }
                }
            }
        }

        return $rules;
    }
}