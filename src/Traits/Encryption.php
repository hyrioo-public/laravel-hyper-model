<?php

namespace Hyrioo\LaravelHyperModel\Traits;

trait Encryption
{
    /**
     * The attributes that must be encrypted when stored
     * @var array
     */
    public $encrypted = [

    ];

    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if (in_array($key, $this->encrypted)) {
            $value = Crypt::decrypt($value);
        }

        return $value;
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->encrypted)) {
            $value = Crypt::encrypt($value);
        }

        return parent::setAttribute($key, $value);
    }

    public function toArray()
    {
        if ($this->hasTransformer()) {
            $data = ($this->transformer)::transform($this);
        } else {
            $data = parent::toArray();
        }


        // Reassign values for encrypt attributes to trigger getAttribute method, except attributes that are hidden
        $encryptedKeys = array_diff_key(array_flip($this->encrypted), array_flip($this->getHidden()));
        foreach ($encryptedKeys as $key => $value) {
            $data[$key] = $this[$key];
        }

        return $data;
    }
}