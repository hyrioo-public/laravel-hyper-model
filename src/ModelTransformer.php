<?php

namespace Hyrioo\LaravelHyperModel;

use Hyrioo\LaravelHyperModel\Models\Model;
use Illuminate\Database\Eloquent\Collection;

class ModelTransformer
{
    public static function transform(Model $model){
        $result = [];
        $result = static::appendLoadedRelations($model, $result);
        return $result;
    }

    protected static function appendLoadedRelations(Model $model, $data)
    {
        foreach ($model->relationships as $relationship){
            if($model->relationLoaded($relationship)){
                $data[snake_case($relationship)] = $model->$relationship->toArray();
            }
        }

        return $data;
    }

    public static function transformCollection(Collection $models){
        $result = [];

        foreach ($models as $model){
            $result[] = static::transform($model);
        }

        return $result;
    }
}