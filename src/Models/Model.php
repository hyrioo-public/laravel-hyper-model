<?php


namespace Hyrioo\LaravelHyperModel\Models;

use Hyrioo\LaravelHyperModel\Traits\Encryption;
use Hyrioo\LaravelHyperModel\Traits\HasRules;
use Hyrioo\LaravelHyperModel\Traits\Transformation;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\Relations\Relation;


/**
 * App\Models\Model
 *
 * @mixin \Eloquent
 */
class Model extends \Illuminate\Database\Eloquent\Model
{
    Use Encryption, Transformation, HasRules;
	/**
	 * The relationships for this model
	 * @var array
	 */
	public $relationships = [

	];


	//////////////////////
	// Creation

	public static function createFromJson($input)
	{
		$model = new static((array)$input);
		$model->associateOneRelationships($input);
		$model->associateMorphRelationships($input);
		$model->save();
		$model->associateManyRelationships($input);

		return $model;
	}

	public function editFromJson($input)
	{
		$this->update($input);
		$this->associateOneRelationships($input);
		$this->associateMorphRelationships($input);
		$this->save();
		$this->associateManyRelationships($input);

		return $this;
	}

    protected function associateOneRelationships($input)
	{
		// Associate one-relationships
		foreach ($this->relationships as $relationship) {
			if (!array_key_exists($relationship, $input)) {
				continue;
			}
			$array = $input[$relationship];

			$class = get_class($this->$relationship());
			if ($class == BelongsToMany::class) {
				continue;
			} else if ($class == BelongsTo::class) {
				if (is_array($array)) {
					/** @var Model $relation_class */
					$relation_class = get_class($this->$relationship()->getModel());
					$keyName = $this->$relationship()->getModel()->getKeyName();
					if (array_key_exists($keyName, $array)) {
						$item = (new $relation_class)->find($array[$keyName]);
						$array = $item->editFromJson($array)->id;
					} else {
						$array = $relation_class::createFromJson($array)->id;
					}
				}
				$this->$relationship()->associate($array);
			}
		}
	}

    protected function associateMorphRelationships($input)
	{
		// Associate one-relationships
		foreach ($this->relationships as $relationship) {
			if (!array_key_exists($relationship, $input)) {
				continue;
			}
			$array = $input[$relationship];

			$class = get_class($this->$relationship());
			if ($class != MorphTo::class) {
				continue;
			} else if ($class == MorphTo::class) {
				/** @var Model $typeClass */
				if (is_array($array)) {
					if ($this->$relationship != null) {
						$typeClass = get_class($this->$relationship->getModel());
						$item = (new $typeClass)->find($this->$relationship->getKey());
						$array = $item->editFromJson($array);
					} else {
						$typeClass = Relation::getMorphedModel($input[$relationship.'_type']);
						$array = $typeClass::createFromJson($array);
					}
				}else{
					$typeClass = Relation::getMorphedModel($input[$relationship.'_type']);
					$array = $typeClass->findOrFail($array);
				}
				$this->$relationship()->associate($array);
			}
		}
	}

    protected function associateManyRelationships($input)
    {
        // Sync many-relationships
        foreach ($input as $relationship => $data) {
            $type = 'sync';
            if ($relationship[0] === '+' || $relationship[0] === '-') {
                $type = $relationship[0] === '+' ? 'attach' : 'detach';
                $relationship = substr($relationship, 1, strlen($relationship) - 1);
            }
            if (!in_array($relationship, $this->relationships)) {
                continue;
            }

            $class = get_class($this->$relationship());
            if ($class == BelongsToMany::class|| $class == MorphToMany::class) {
                $relations = [];
                foreach ($data as $item) {
                    if (is_array($item)) {
                        /** @var Model $relation_class */
                        $relation_class = get_class($this->$relationship()->getModel());
                        $keyName = $this->$relationship()->getModel()->getKeyName();
                        if (array_key_exists($keyName, $item)) {
                            $i = (new $relation_class)->find($item[$keyName]);
                            $relations[] = $i->editFromJson($item)->id;
                        } else {
                            $relations[] = $relation_class::createFromJson($item)->id;
                        }
                    } else {
                        $relations[] = $item;
                    }
                }
                $this->$relationship()->$type($relations);
            }else if ($class == HasMany::class) {
                $relations = [];
                /** @var Model $relation_class */
                $relation_class = get_class($this->$relationship()->getModel());
                $keyName = $this->$relationship()->getModel()->getKeyName();

                foreach ($data as $item) {
                    if (is_array($item)) {
                        $item[$this->$relationship()->getForeignKeyName()] = $this->getKey();
                        if (array_key_exists($keyName, $item)) {
                            $i = (new $relation_class)->find($item[$keyName]);
                            $relations[] = $i->editFromJson($item)->id;
                        }
                    }else{
                        $relations[] = $item;
                    }
                }
                // Manually delete existing objects that is no longer present
                // TODO: Support detach/attach
                $this->$relationship->filter(function($i) use($keyName, $relations){
                    return !in_array($i->$keyName, $relations);
                })->each->delete();

                foreach ($data as $item) {

                    if (is_array($item)) {
                        $item[$this->$relationship()->getForeignKeyName()] = $this->getKey();
                        if (!array_key_exists($keyName, $item)) {
                            $relations[] = $relation_class::createFromJson($item)->id;
                        }
                    }
                }

            } else if ($class == BelongsTo::class || $class == MorphTo::class) {
                continue;
            }
        }
    }
}