<?php

namespace Hyrioo\LaravelHyperModel;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
       /* $this->publishes([
            __DIR__ . '/../resources/config/api.php' => config_path('api.php')
        ], 'config');*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        /*$configPath = __DIR__ . '/../resources/config/api.php';
        $this->mergeConfigFrom($configPath, 'api');*/
    }
}
