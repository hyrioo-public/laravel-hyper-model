# Features

This package allows you to generically create objects with their relations and link them together from a json object with one line of code.

Simply extend your models from the class `Hyrioo\LaravelHyperModel\Models\Model` fill out the `$relationships` array with snake case verions of the models relation methods.
Then call `Model::createFromJson($json)` or `$model->editFromJson($json)` to create/edit an model and it's relations.

```
class Movie extends Model
{
	$public $relationships = [
        'format',
        'actors'
    ];

    //...

    public function actors()
    {
        return $this->belongsToMany(Actor::class, 'movie_actors')->withTimestamps();
    }
    public function format()
    {
        return $this->belongsTo(Format::class, 'format_id', 'id');
    }
}
```

If an object in the relation does not contain the primary key, it will assume a new obejct should be created. By including the primary key, you can update an existing object while linking it to the root object:
```
Movie object:
{
	"title: "Molestias aut",
    "format": {
    	"format": "IMAX"
    }
}
Movie object:
{
	"title: "Molestias aut",
    "format": {
    	"id": 1,
    	"format": "IMAX Legacy"
    }
}
```

By default relations will use the sync method, meaning the relations you include in the json will be the only relations linked to the model. By putting a `-` or `+` in front of the relation key, you can change the behavior til `detach` and `attach`:

```
Movie object:
{
	"title: "Molestias aut",
    "-actors": [
    	1,3
    ]
}
```


# How to use

## Tables used in examples

| movies        |   | actors        |   | movie_actors |   | formats |
|---------------|---|---------------|---|--------------|---|---------|
| id            |   | id            |   | id           |   | id      |
| title         |   | first_name    |   | movie_id     |   | format  |
| description   |   | last_name     |   | actor_id     |   |         |
| release_year  |   | *full_name*   |   |              |   |         |
| length        |   | birthday      |   |              |   |         |
| format_id     |   | birth_country |   |              |   |         |
| main_language |   |               |   |              |   |         |

**italic means calculated field*


## Examples

#### Actor object
This example will simply create an actor object, with no relations.
```json
Actor object:
{
    "first_name": "Teagan",
    "last_name": "Hartmann",
    "birthday": "1972-04-20",
    "birth_country": "IM"
}
```

#### Movie object with a new actor object (hasMany)
This example creates a movie and an actor at the same time, and creates a movie_actor relation between the two.
```json
Movie object:
{
	"title": "molestias aut",
    "description": "Ea facere a culpa voluptas ut. Beatae nostrum quia dolorem debitis quis officia ullam.",
    "release_year": 1941,
    "length": 109,
    "actors": [
      	{
            "first_name": "Teagan",
            "last_name": "Hartmann",
            "birthday": "1972-04-20",
            "birth_country": "IM"
    	}
	]
}
```


#### Movie object with a exiting actor id (hasMany)
This example creates a movie and creates a movie_actor relation to an existing actor.
```json
Movie object:
{
	"title": "Molestias aut",
    "description": "Ea facere a culpa voluptas ut. Beatae nostrum quia dolorem debitis quis officia ullam.",
    "release_year": 1941,
    "length": 109,
    "actors": [
    	1
	]
}
```

#### Movie object while updating an actor (hasMany)
This example creates a movie and creates a movie_actor relation to an existing actor while updating specified fields on the actor.
```json
Movie object:
{
	"title": "Molestias aut",
    "description": "Ea facere a culpa voluptas ut. Beatae nostrum quia dolorem debitis quis officia ullam.",
    "release_year": 1941,
    "length": 109,
    "actors": [
    	{
        	"id": 1,
            "last_name": "Hartmann-Vandervort",
    	}
	]
}
```

#### Movie object with mixed actor objects (hasMany)
This example creates a movie, updates one actor, creates a new actor, and creates movie_actor relations to all three actors.
```json
Movie object:
{
	"title": "Molestias aut",
    "description": "Ea facere a culpa voluptas ut. Beatae nostrum quia dolorem debitis quis officia ullam.",
    "release_year": 1941,
    "length": 109,
    "actors": [
    	{
        	"id": 1,
            "last_name": "Hartmann-Vandervort",
    	},
        2,
        {
            "first_name": "Teagan",
            "last_name": "Hartmann",
            "birthday": "1972-04-20",
            "birth_country": "IM"
    	}
	]
}
```

#### Movie object with format object (hasOne)
This example creates a movie and creates a new format and sets the `format_id` on the created movie object
```json
Movie object:
{
	"title": "Molestias aut",
    "description": "Ea facere a culpa voluptas ut. Beatae nostrum quia dolorem debitis quis officia ullam.",
    "release_year": 1941,
    "length": 109,
    "format": {
    	"format": "IMAX"
    }
}
```